package com.example.intentsbroadcastlab;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

public class CambioMoneda extends Activity {
	
	private String moneda;
	// Codigo clave para iniciar la camara
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cambio_moneda);
		
		// Obtener el intent y los datos enviados desde el Main
        // code here
        
        // Mostrar el valor de COP enviado por el usuario
        TextView copText = (TextView) findViewById(R.id.cop);
        copText.setText("COP: " +  cop);
        
        // Calcular el valor de la moneda escogida por el usuario
        double calculoMoneda = Math.round(calcularMoneda(copValor, moneda)* 100.0) / 100.0;
        
        
        // Mostrar el valor de la moneda calculada
        TextView eurText = (TextView) findViewById(R.id.moneda);
        eurText.setText(moneda + ": " + calculoMoneda);
	}
	
	// Dependiendo de la eleccion del usuario desplega una pagina web con informacion
	// referente  a la divisa seleccionada.
	public void intentBrowser (View v) {
		
			//url
		
	}
	
	// Lanza una llamada al numero telefonico indicado
	public void intentCall(View v) {
		
		//call
			
	}
	
	// Despliega la camara del dispositivo movil
	public void smilePhoto(View v) {
		
		//camera
		
	}
	// Cambio de tasas
	private double calcularMoneda(double cop, String moneda) {
		if(moneda.equals("EUR"))
				return cop/2563.91;
		if(moneda.equals("USD"))
				return cop/1946.59;
		return 0;
	}

}
