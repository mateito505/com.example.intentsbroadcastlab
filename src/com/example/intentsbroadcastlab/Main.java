package com.example.intentsbroadcastlab;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;

public class Main extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
	}
	
	public void cambiarMoneda(View view) {
	    // Obtener los Pesos colombianos desde la vista
		EditText editText = (EditText) findViewById(R.id.editText1);
		String cop = editText.getText().toString();
		
		// Obtener la moneda de cambio seleccionada por el usuario
		RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
		int selected = radioGroup.getCheckedRadioButtonId();
		RadioButton radioButton = (RadioButton) findViewById(selected);
		String moneda = radioButton.getText().toString();
		
		// Crear un intent explicito para enviar los datos a otro Activity
		//code here
		
		// Valida que el usuario entre algun valor, de lo contrario emite un mensaje
		if (editText.getText().toString().equals("")) {
			
			Toast.makeText(this, "No ha ingresado ningún valor, intentelo de nuevo",
		    Toast.LENGTH_LONG).show();
	    
		} else {
			    	
			//code here
			    
	    }
			
	}
	
	// Enviar una alerta a traves del sistema (broadcast)
	public void empezarAlerta(View view) {
		
		// Recupera el valor en segundos para que la alerta se haga
	    EditText text = (EditText) findViewById(R.id.timeText);
	 // Valida que el usuario entre algun valor, de lo contrario emite un mensaje
	    if (text.getText().toString().equals("")) {
		    
	    	Toast.makeText(this, "No ha ingresado ningun valor, intentelo de nuevo",
		    Toast.LENGTH_LONG).show();
	    	
	    } else {
	    	
	    	// Asigna el valor en segundos indicado por el usuario para enviar
	    	// la alerta del sistema (broadcast) a la clase broadcastReceiver
	    	int time = Integer.parseInt(text.getText().toString());
		    // code here
		    Toast.makeText(this, "Alarm set in " + time + " seconds",
		    Toast.LENGTH_LONG).show();
		    
		    // Desaparece los elementos de la interfaz grafica
		    TextView txtView = (TextView) findViewById(R.id.textView3);
		    Button btn = (Button)findViewById(R.id.button2);
		    btn.setVisibility(View.INVISIBLE);
		    text.setVisibility(View.INVISIBLE);
		    txtView.setVisibility(View.INVISIBLE);
	    	
	    }
	    
	}

}
